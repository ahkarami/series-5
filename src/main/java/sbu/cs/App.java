package sbu.cs;

import java.util.ArrayList;
import java.util.List;

public class App {

    /**
     * use this function for magical machine question.
     *
     * @param n     size of machine
     * @param arr   an array in size n * n
     * @param input the input string
     * @return the output string of machine
     */
    public String main(int n, int[][] arr, String input) {
        Machine machine = new Machine(n, input);
        machine.setMachine(arr);
        machine.machineRun();





        return machine.getMachineOutput();
    }
}
