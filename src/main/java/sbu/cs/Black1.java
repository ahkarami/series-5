package sbu.cs;

public class Black1 implements BlackFunction {

    @Override
    public String func(String arg) {

        StringBuilder stringBuilder = new StringBuilder(arg);
        String out = stringBuilder.reverse().toString();

        return out;
    }
}
