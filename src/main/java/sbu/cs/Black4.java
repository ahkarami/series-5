package sbu.cs;

public class Black4 implements BlackFunction {

    @Override
    public String func(String arg) {
        char[] arr = arg.toCharArray();

        String out = "" + arr[arr.length - 1];
        for (int i = 0; i < arr.length - 1; i++) {
            out = out + arr[i];
        }

        return out;
    }
}
