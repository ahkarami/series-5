package sbu.cs;

public class Black5 implements BlackFunction {

    @Override
    public String func(String arg) {
        char[] arr = arg.toCharArray();
        String out = "";

        for (int i = 0; i < arr.length; i++) {
            out = out + reverseEquivalent(arr[i]);
        }


        return out;
    }

    public char reverseEquivalent (char ch) {
        int index = ch - 'a';
        return (char) ('z' - ch + 'a');
    }
}
