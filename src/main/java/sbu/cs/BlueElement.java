package sbu.cs;

public class BlueElement extends Element {

    private BlackFunction func;

    public BlueElement (int funcType) {
        if (funcType == 1) {
            func = new Black1();
        } else if (funcType == 2) {
            func = new Black2();
        } else  if (funcType == 3) {
            func = new Black3();
        } else if (funcType == 4) {
            func = new Black4();
        } else if (funcType == 5) {
            func = new Black5();
        }

    }

    @Override
    public void run() {
        setRightOutput(func.func(getLeftInput()));
        setDownOutput(func.func(getUpInput()));
    }
}
