package sbu.cs;

public class Element {
    private String leftInput;
    private String upInput;
    private String rightOutput;
    private String downOutput;
    private int number;

    public void setLeftInput(String leftInput) { this.leftInput = leftInput; }

    public void setUpInput(String upInput) { this.upInput = upInput; }

    public void setDownOutput(String downOutput) { this.downOutput = downOutput; }

    public void setRightOutput(String rightOutput) { this.rightOutput = rightOutput; }

    public void setNumber(int number) { this.number = number; }

    public String getLeftInput() { return leftInput; }

    public String getUpInput() { return upInput; }

    public String getRightOutput() { return rightOutput; }

    public String getDownOutput() { return downOutput; }

    public int getNumber() { return number; }
//    ***********************************************



    public void run () {}
}
