package sbu.cs;

public class GreenElement extends Element{

    private BlackFunction func;

    public GreenElement(int funcType) {
        if (funcType == 1) {
            func = new Black1();
        } else if (funcType == 2) {
            func = new Black2();
        } else  if (funcType == 3) {
            func = new Black3();
        } else if (funcType == 4) {
            func = new Black4();
        } else if (funcType == 5) {
            func = new Black5();
        }

    }

    @Override
    public void run() {
        setDownOutput(func.func(getUpInput()));
        setRightOutput(func.func(getUpInput()));
    }
}
