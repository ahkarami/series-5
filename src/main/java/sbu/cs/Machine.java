package sbu.cs;

public class Machine {
    private Element[][] machine;
    private int size;
    private String machineInput;
    private String machineOutput;

    public Machine(int size, String machineInput) {
        this.size = size;
        this.machineInput = machineInput;
        this.machine = new Element[size][size];
    }

    public String getMachineOutput() {
        return machineOutput;
    }

    public void setMachine (int[][] arr) {
        machine[0][0] = new GreenElement(arr[0][0]);
        for (int j = 1; j < size - 1; j++) {
            machine[0][j] = new GreenElement(arr[0][j]);
        }
        for (int i = 1; i < size - 1; i++) {
            machine[i][0] = new GreenElement(arr[i][0]);
        }
        machine[0][size - 1] = new YellowElement(arr[0][size - 1]);
        machine[size - 1][0] = new YellowElement(arr[size - 1][0]);

        for (int i = 1; i < size - 1; i++) {
            for (int j = 1; j < size - 1; j++) {
                machine[i][j] = new BlueElement(arr[i][j]);
            }
        }

        for (int i = 1; i < size - 1; i++) {
            machine[i][size - 1] = new PinkElement(arr[i][size - 1]);
        }
        for (int j = 1; j < size - 1; j++) {
            machine[size - 1][j] = new PinkElement(arr[size - 1][j]);
        }
        machine[size - 1][size - 1] = new PinkElement(arr[size - 1][size - 1]);
    }

    public void machineRun () {
        machine[0][0].setUpInput(machineInput);
        machine[0][0].run();
        // up greens
        for (int j = 1; j < size - 1; j++) {
            machine[0][j].setLeftInput(machine[0][j - 1].getRightOutput());
            machine[0][j].setUpInput(machine[0][j - 1].getRightOutput());
            machine[0][j].run();
        }

        // left greens
        for (int i = 1; i < size - 1; i++) {
            machine[i][0].setLeftInput(machine[i - 1][0].getDownOutput());
            machine[i][0].setUpInput(machine[i - 1][0].getDownOutput());
            machine[i][0].run();
        }

        // blues
        for (int i = 1; i < size - 1; i++) {
            for (int j = 1; j < size - 1; j++) {
                machine[i][j].setLeftInput(machine[i][j - 1].getRightOutput());
                machine[i][j].setUpInput(machine[i - 1][j].getDownOutput());
                machine[i][j].run();
            }
        }

        // yellows
        machine[0][size - 1].setLeftInput(machine[0][size - 2].getRightOutput());
        machine[0][size - 1].setUpInput(machine[0][size - 2].getRightOutput());
        machine[0][size - 1].run();


        machine[size - 1][0].setUpInput(machine[size - 2][0].getDownOutput());
        machine[size - 1][0].setLeftInput(machine[size - 2][0].getDownOutput());
        machine[size - 1][0].run();

        // down pinks
        for (int j = 1; j < size - 1; j++) {
            machine[size - 1][j].setLeftInput(machine[size - 1][j - 1].getRightOutput());
            machine[size - 1][j].setUpInput(machine[size - 2][j].getDownOutput());
            machine[size - 1][j].run();
        }
        // right pinks
        for (int i = 1; i < size - 1; i++) {
            machine[i][size - 1].setLeftInput(machine[i][size - 2].getRightOutput());
            machine[i][size - 1].setUpInput(machine[i - 1][size - 1].getDownOutput());
            machine[i][size - 1].run();
        }

        // last element
        machine[size - 1][size - 1].setUpInput(machine[size - 2][size - 1].getDownOutput());
        machine[size - 1][size - 1].setLeftInput(machine[size - 1][size - 2].getRightOutput());
        machine[size - 1][size - 1].run();

        machineOutput = machine[size - 1][size - 1].getDownOutput();
    }
}
