package sbu.cs;

public class PinkElement extends Element {
    private WhiteFunction func;

    public PinkElement (int funcType) {
        if (funcType == 1) {
            func = new White1();
        } else if (funcType == 2) {
            func = new White2();
        } else if (funcType == 3) {
            func = new White3();
        } else if (funcType == 4) {
            func = new White4();
        } else if (funcType == 5) {
            func = new White5();
        }

    }

    @Override
    public void run() {
        setRightOutput(func.func(getLeftInput(),getUpInput()));
        setDownOutput(func.func(getLeftInput(),getUpInput()));
    }
}
