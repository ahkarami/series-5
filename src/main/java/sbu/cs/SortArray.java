package sbu.cs;

import java.util.ArrayList;
import java.util.List;

public class SortArray {

    /**
     * sort array arr with selection sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] selectionSort(int[] arr, int size) {
        int [] out = arr.clone();
        for (int i = 0; i < size - 1; i++) {
            for (int j = i + 1; j < size; j++) {
                if (out[j] < out[i]) {
                    int tmp = out[i];
                    out[i] = out[j];
                    out[j] = tmp;
                }
            }
        }
        return out;
    }

    /**
     * sort array arr with insertion sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] insertionSort(int[] arr, int size) {
        int[] out = arr.clone();

        for (int i = 1; i < size; i++) {
            int key = out[i];
            int index = i - 1;
            while (index >= 0 && out[index] > key) {
                out[index + 1] = out[index];
                index--;
            }
            out[index + 1] = key;
        }
        return out;
    }

    /**
     * sort array arr with merge sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] mergeSort(int[] arr, int size) {
        int [] out = arr.clone();
        sort(out, 0, out.length - 1);

        return out;
    }

    private void sort (int[] arr, int beg, int end) {
        if (end == beg) {
            return;
        }
        if (beg < end) {
            int middle = (int) Math.floor((beg + end)/2.0);
            sort(arr,beg,middle);
            sort(arr,middle + 1, end);
            merge(arr, beg, middle, end);
        }
    }

    private void merge (int[] arr, int first, int middle, int end) {
        List<Integer> arr1 = new ArrayList<>();
        List<Integer> arr2 = new ArrayList<>();

        for (int i = first; i < middle + 1; i++) {
            arr1.add(arr[i]);
        }
        for (int i = middle + 1; i <= end; i++) {
            arr2.add(arr[i]);
        }

        int index1 = 0, index2 = 0, p = first;
        while (index1 < arr1.size() && index2 < arr2.size()) {
            if (arr1.get(index1) <= arr2.get(index2)) {
                arr[p] = arr1.get(index1);
                index1++;
            } else {
                arr[p] = arr2.get(index2);
                index2++;
            }
            p++;
        }

        while (index1 < arr1.size()) {
            arr[p] = arr1.get(index1);
            index1++;
            p++;
        }
        while (index2 < arr2.size()) {
            arr[p] = arr2.get(index2);
            index2++;
            p++;
        }


    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in iterative form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearch(int[] arr, int value) {
        int l = 0, r = arr.length - 1;
        while (l <= r) {
            int m = l + (r - l) / 2;

            if (arr[m] == value) {
                return m;
            }
            if (arr[m] < value) {
                l = m + 1;
            } else {
                r = m - 1;
            }
        }


        return -1;
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in recursive form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearchRecursive(int[] arr, int value) {
        return binaryRecursive(arr, 0, arr.length - 1, value);
    }

    private int binaryRecursive (int[] arr, int l, int r, int value) {

        if (r >= l) {
            int mid = l + (r - l) / 2;

            // If the element is present at the
            // middle itself
            if (arr[mid] == value)
                return mid;

            // If element is smaller than mid, then
            // it can only be present in left subarray
            if (arr[mid] > value)
                return binaryRecursive(arr, l, mid - 1, value);

            // Else the element can only be present
            // in right subarray
            return binaryRecursive(arr, mid + 1, r, value);
        }

        // We reach here when element is not present
        // in array
        return -1;
    }
}
