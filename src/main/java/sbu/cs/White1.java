package sbu.cs;


public class White1 implements WhiteFunction {

    @Override
    public String func(String arg1, String arg2) {
//        input1 = arg1;
//        input2 = arg2;

        char[] arr1 = arg1.toCharArray();
        char[] arr2 = arg2.toCharArray();

        String out = "";

        int i = 0;
        while (i < arr1.length && i < arr2.length) {
            out = out + arr1[i];
            out = out + arr2[i];

            i++;
        }

        if (i == arr1.length) {
            out = out + arg2.substring(i);
        } else if (i == arr2.length) {
            out = out + arg1.substring(i);
        }

//        output = out;
        return out;

    }
}
