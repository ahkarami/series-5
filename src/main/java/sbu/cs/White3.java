package sbu.cs;

public class White3 implements WhiteFunction {


    @Override
    public String func(String arg1, String arg2) {

        String reversedArg2 = new StringBuilder(arg2).reverse().toString();

        char[] arr1 = arg1.toCharArray();
        char[] arr2 = reversedArg2.toCharArray();

        String out = "";
        int i = 0;
        while (i < arr1.length && i < arr2.length) {
            out = out + arr1[i] + arr2[i];
            i++;
        }

        if (i == arr1.length) {
            out = out + reversedArg2.substring(i);
        } else if (i == arr2.length) {
            out = out + arg1.substring(i);
        }

        return out;

    }
}
