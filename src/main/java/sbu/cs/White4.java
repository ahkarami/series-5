package sbu.cs;

public class White4 implements WhiteFunction {
    private String input1;
    private String input2;
    private String output;

    @Override
    public String func(String arg1, String arg2) {
//        input1 = arg1;
//        input2 = arg2;

        String out;

        if (arg1.length() % 2 == 0) {
            out = arg1;
        } else {
            out = arg2;
        }

//        output = out;
        return out;

    }
}
