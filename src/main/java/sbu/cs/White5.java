package sbu.cs;

public class White5 implements WhiteFunction {


    @Override
    public String func(String arg1, String arg2) {
        String out = "";

        char[] arr1 = arg1.toCharArray();
        char[] arr2 = arg2.toCharArray();

        int i = 0;
        while (i < arr1.length && i < arr2.length) {
            out = out + (char) (((arr1[i] + arr2[i] - 2*'a') % 26) + 'a');
            i++;
        }

        if (i == arr1.length) {
            out = out + arg2.substring(i);
        } else if (i == arr2.length) {
            out = out + arg1.substring(i);
        }


        return out;

    }
}
